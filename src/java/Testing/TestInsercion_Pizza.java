/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testing;

import DAO.Conexion;
import DAO.PizzaJpaController;
import DTO.Pizza;
import DTO.Sabor;
import DTO.Tipo;

/**
 *
 * @author madar
 */
public class TestInsercion_Pizza {
    
    public static void main(String[] args) {
        Conexion con=Conexion.getConexion();
        PizzaJpaController pizzaDAO=new PizzaJpaController(con.getBd());
        Pizza nuevaPizza=new Pizza();
        nuevaPizza.setValor(200000);
        Tipo tipo=new Tipo();
        tipo.setId(1);
        Sabor sabor=new Sabor();
        sabor.setIdSabor(2);
        
        nuevaPizza.setIdTipo(tipo);
        nuevaPizza.setIdSabor(sabor);
        
        pizzaDAO.create(nuevaPizza);
        System.out.println("Creando una pizza ");
        
        
    }
}
