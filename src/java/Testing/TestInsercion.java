/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testing;

import DAO.Conexion;
import DAO.PizzaJpaController;
import DAO.SaborJpaController;
import DTO.Sabor;

/**
 *
 * @author madar
 */
public class TestInsercion {
    
    public static void main(String[] args) {
        Conexion con=Conexion.getConexion();
        SaborJpaController saborDAO=new SaborJpaController(con.getBd());
        Sabor saborNuevo=new Sabor();
        saborNuevo.setIdSabor(2);
        saborNuevo.setDescripcion("Bechamel");
        try{
            saborDAO.create(saborNuevo);
            System.out.println("Objeto Creado sabor");
        }catch(Exception e)
        {
            System.out.println("No se pudo crear...");
        }
        
    }
    
}
